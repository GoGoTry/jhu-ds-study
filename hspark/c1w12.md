# Data Scientist Toolbox

## week1

### Specialization Motivation

why data science?

* data deluge & 데이터 활용 툴 발전s
* 관련 문서 : https://www.economist.com/node/15579717

why statistical data science?

* 데이터를 통해서 아주 깔끔하고 명확한 결론을 얻는 경우는 드뭄
* 불확실성을 다루기 위해서 통계가 필요
* 관련 문서 : https://www.nytimes.com/2009/08/06/technology/06stats.html?_r=0

our goal

* Hacking skills
	* computer programming / 답을 찾아내는 능력 (검색 등)
* Math & Statistics Knowledge
* Substantive Expertise

### Getting Help / Finding Answers

In R :
```{r}
?rnorm
help.search("rnorm")
args("rnorm")
```
질문할때 좋은 방법 / 에티켓:

* 배경 정보 제공 / 목적을 서술 / 버전 등을 명시

Key characteristic of hackers

* 스스로 답을 찾으려고 한다
* 어디서 정보를 얻는지 안다
* 새로운 데이터에 위축되지 않는다
* 답을 모르는 것을 두려워 하지 않는다
* 정중하지만 끈질기다

### Specialization course overview

### Install R / R Studio On Mac

install R:
>https://cran.r-project.org/bin/macosx/  

install R Studio:
>https://www.rstudio.com/products/rstudio/download/

참고:
>https://www.andrewheiss.com/blog/2012/04/17/install-r-rstudio-r-commander-windows-osx/

## week2 

### Command Line Interface

```sh
pwd
ls 
ls -a
ls -al
cd
mkdir
touch
cp [-r]
rm [-r]
mv
date
echo
```


### Introduction to Git / GitHub
install git:
> https://git-scm.com/downloads

config:
```sh
git config --global user.name "your name"
git config --global user.email "your_email@email.com"
git config --list
```
github:
> https://github.com/

### Basic git command
overview:
>https://github.com/DataScienceSpecialization/courses/blob/master/assets/img/01_DataScientistToolbox/gghuboverview.png

command:
```sh
git add .
git add -u
git add -A

git commit -m "msg"

git push origin master

git checkout -b branchname

git pull
git clone
```

### Basic Markdown


### Install R Packages

```r
a <- avaiable.packages()
head(rownames(a), 3)

install.packages("pkg_name")
install.packages(c("pkg_name", "pkg_b", "pkg_c"))

source("http://bioconductor.org/biocLite.R")
biocLite()

biocLite(c("pkg_a", "pkg_b"))

library(ggplot2)
```

### Install R tools

### Week1 Quiz

1. Question 1  
Which of the following are courses in the Data Science Specialization? Select all that apply.
	- [x] Practical Machine Learning
	- [ ] Data Science 101
	- [ ] Business Analytics
	- [x] Exploratory Data Analysis
	- [ ] Machine Learning for Hackers

2. Question 2  
Why are we using R for the course track? Select all that apply.
	- [x] R has a nice IDE, Rstudio.
	- [ ] R allows object oriented programming.
	- [x] R has a large number of add on packages that are useful for data analysis.
	- [ ] R is a general purpose programming language.
	- [x] R is free.

3. Question 3  
What are good ways to find answers to questions in this course track? Select all that apply.
	- [ ] Emailing the community TAs
	- [x] Searching Google.
	- [x] Posting to the course discussion forum
	- [ ] Posting homework assignments to mailing lists

4. Question 4  
What are characteristics of good questions on the message boards? Select all that apply.
	- [ ] Provides no details.
	- [x] Describes the goal of the analysis.
	- [ ] Assumes that you've discovered a bug in R.
	- [ ] Begs for help without providing information.
	- [x] Explicitly lists versions of software being used.
	- [x] Provides the minimum amount of information necessary to communicate the problem.

5. Question 5  
Which of the following packages provides machine learning functionality? Select all that apply
	- [ ] knitr
	- [ ] shiny
	- [x] gbm
	- [ ] pamr


### Week2 Quiz

1. Which of the following commands will create a directory called data in your current working directory?
	- [ ] cd data
	- [x] mkdir data
	- [ ] mkdir /Users/data
	- [ ] pwd ../data

2. Which of the following will initiate a git repository locally?
	- [ ] git boom
	- [x] git init
	- [ ] git remote add
	- [ ] git merge origin master

3. Suppose you have forked a repository called datascientist on Github but it isn't on your local computer yet. Which of the following is the command to bring the directory to your local computer?
(For this question assume that your user name is username)
	- [ ] git pull https://github.com/username/datascientist.git
	- [x] git clone https://github.com/username/datascientist.git
	- [ ] git init
	- [ ] git pull datascientist master

4. Which of the following will create a markdown document with a secondary heading saying "Data Science Specialization" and an unordered list with the following for bullet points: Uses R, Nine courses, Goes from raw data to data products
	- [ ]  
```
### Data Science Specialization

* Uses R

* Nine courses

* Goes from raw data to data products
```
	- [ ] 
```
## Data Science Specialization

li Uses R

li Nine courses

li Goes from raw data to data products
```
	- [x] 
```
## Data Science Specialization

* Uses R

* Nine courses

* Goes from raw data to data products
```
	- [ ] 
```
*h2 Data Science Specialization

* Uses R

* Nine courses

* Goes from raw data to data products
```
	- [ ] 
```
*** Data Science Specialization

* Uses R

* Nine courses

* Goes from raw data to data products
```
5. Install and load the KernSmooth R package. What does the copyright message say?
 	- [ ] Copyright Matthew Wand 1997-2009
 	- [ ] Copyright M. P. Wand 1997-2013
 	- [x] Copyright M. P. Wand 1997-2009
 	- [ ] Copyright M. P. Wand